# SPRINTFUCK v1.0.5 -  WRITTEN BY BLUNT MOAK FROG
# 7-ELEVEN HEALTH INSPECTION AGENCY AND CHRIS HANSEN SWAT UNIT 2022
import time
import random
import os
import pyautogui
from pynput import keyboard

# Class of variables to be read within the program
# Message stores the spammed message, speed stores timeout delay
# Foo stores boolean for closing SprintFuck, Bar stores boolean for speed toggle
class fooBar():
    global message
    global arg
    speed = int(1)
    caseSwitch = int(0)
    foo = True

# AutoSpam Plugin Variables
class AutoSpam():
    sdn = "https://i.imgur.com/hTMlexc.jpg"
    toiletGuy = "https://i.imgur.com/H2fLz5F.jpg"
    skillIssue = "https://i.imgur.com/Mfad68h.gif"
    hukPoby = ":frog: huk poby bribit :frog:"
    hereForFroge = "https://i.imgur.com/4O2LfpU.png"
    whyHeLookin = "https://www.youtube.com/watch?v=06fonBCvSRg"
    muscleManDonuts = "https://www.youtube.com/watch?v=W2D2LubkdyY"
    pingEveryone = "@everyone"

# BotPwn Plugin Variables
class BotPwn():
    botPwnSyntax = ""
    botPwnList = []
    botPwnCase = 0
    botPwnCmd = ""
    botPwnCheck = ""
    botPwnSpam = ""

# Pandora's Box Plugin Variables
class PandorasBox():
    surprise = ""
    pandorasBoxVars = ["https://i.imgur.com/ACPkPw4.jpeg", "https://i.imgur.com/hLQyRCl.jpg", "https://i.imgur.com/XpeB3sM.jpg", 
                       "https://i.imgur.com/gntwB3H.jpg", "https://i.imgur.com/RYeFuw2.jpg", "https://i.imgur.com/5PvTTVu.jpg", 
                       "https://i.imgur.com/bL9L3la.jpg", "https://i.imgur.com/4k4vs44.jpg", "https://i.imgur.com/nKBEm0z.jpg", 
                       "https://i.imgur.com/7NJKjWR.jpg", "https://i.imgur.com/syCWNNb.jpg", "https://i.imgur.com/GEAY1IW.jpg", 
                       "https://i.imgur.com/KUsfS5L.jpg", "https://i.imgur.com/hcXfbjV.png", "https://i.imgur.com/EQ6qXlP.jpg"]

# Sporky Plugin Variables
class Sporky():
    sporky = ""
    sporkyRandom = ""
    sporkyAutofill = ["https://gfycat.com/warmselfassuredkilldeer", "https://gfycat.com/boilingmarriedcalf",
                      "https://gfycat.com/windinghighlevelchimpanzee", "https://gfycat.com/paltrydifficultalbertosaurus",
                      "https://gfycat.com/gleefulclumsyarcticfox", "https://shorturl.at/hvGOS"]

# Storing boolean values
class booleans():
    discordModuleCheck = ""
    discordModuleOn = False
    botPwnCheck = ""
    botPwnAddList = True
    botPwnOn = False
    botPwnToggleOn = False
    sporkyCheck = ""
    sporkyOn = False
    sporkyAutofillOn = False
    pandorasBoxOn = False
    sdnOn = False
    toiletGuyOn = False
    skillIssueOn = False
    hukPobyOn = False
    hereForFrogeOn = False
    whyHeLookinOn = False
    muscleManDonutsOn = False
    pingEveryoneOn = False
    spamOn = False

# The Discord Module :D
def moduleCheck():
    booleans.discordModuleCheck = input("Will you be using the Discord Module? Type yes or no then press enter: ")
    if booleans.discordModuleCheck == "yes" or booleans.discordModuleCheck == "Yes":
        booleans.discordModuleOn = True
        booleans.botPwnCheck = input("Will you be using the BotPwn Plugin? Type yes or no then press enter: ")
        if booleans.botPwnCheck == "yes" or booleans.botPwnCheck == "Yes":
            booleans.botPwnOn = True
            BotPwn.botPwnSyntax = input("Enter the syntax for the Bot you wish to exploit: ")
            while booleans.botPwnAddList == True:
                BotPwn.botPwnCheck = input("Enter the Bot command you wish to spam, type done if you're done: ")
                if BotPwn.botPwnCheck == "done" or BotPwn.botPwnCheck == "Done":
                    booleans.botPwnAddList = False
                else:
                    BotPwn.botPwnList.append(BotPwn.botPwnCheck)
        elif booleans.botPwnCheck == "no" or booleans.botPwnCheck == "No":
            pass
        booleans.sporkyCheck = input("Do you wish to set a custom Fork Bomb for Sporky? Type yes or no then press enter: ")
        if booleans.sporkyCheck == "yes" or booleans.sporkyCheck == "Yes":
            Sporky.sporky = input("Set your custom Fork Bomb: ")
        elif booleans.sporkyCheck == "no" or booleans.sporkyCheck == "No":
            booleans.sporkyAutofillOn = True
    elif booleans.discordModuleCheck == "no" or booleans.discordModuleCheck == "No":
        booleans.discordModuleOn = False
    else:
        print("Check failed... Try again...")
        moduleCheck()

# The SprintFuck Menu is now a function itself, we've come so far :')
def mainMenu():
    time.sleep(1)
    print('''
FROM THE FROG SQUAD COMES A NEW LULZ TOOL...
''')
    time.sleep(1)
    print('''
PRESENTING SOME QUALITY SHITWARE TO FUCK YOUR ASS THE RIGHT WAY
''')
    time.sleep(1)
    print ('''
IT'S...
                   _____            _       __  ______           __    
                  / ___/____  _____(_)___  / /_/ ____/_  _______/ /__  
                  \__ \/ __ \/ ___/ / __ \/ __/ /_  / / / / ___/ //_/  
                 ___/ / /_/ / /  / / / / / /_/ __/ / /_/ / /__/ ,<     
                /____/ .___/_/  /_/_/ /_/\__/_/    \__,_/\___/_/|_|    
                    /_/                                                                                            
''')
    time.sleep(0.5)
    print ('''
                                                    @@O@                                       @@@@@@@@@@@@                                 
                                   @              @     O                                    @@            @@@@                             
                                  @ O@@             @@@# @                                 @@  @@@@@@@@@@@     @@@@@@@@@                    
                                 @ °   O@      O @@@@@@@@ @                             @@   @@@@@@@@@@@@@@@@@          @@@@@               
                                @ @@@#O  O  OO.  O*O#@#@@# @@@                         @   @@@@           *O@@@@@@@@@@@@     @@@@@@@@@      
                               O @@° .#O@      @#* oo#@@@@O   @@@O                    @ @@@@O                 *###@@@@@@@@@@@               
                              @@@@o.*o#@@@  Oo°.°*oooOO#@@@@@#     @@               @@ @@@*                            *@@@@@@@@@@@@@@@@    
                               @@@Oo*ooO##°.**.°°*°***oOO#@@@@@@@#   O              @@@@.                                   oOO@@@@@@@@@    
                           @O@  #Ooooo***°****oo*******oOO#@@@@@@@@@o @            @ @@°   #          #                 **         .@@ @    
                        @O°     oo***oo°°****ooooooooooOOO#@@@@@@@@@@@ @           @ @@    @oO* *.  o @ O°  .#ooO  *ooO @@oO***  O  @@@@@   
                      O    @@@#O*°°°°oo**°°°°°°**oooOOoooO@@@@@@@@@@@@@ @          @ @@   o@  @.@  o#*@#°   @O  @OoO  ##@.  @o@ @.  o@@ @   
                @@@@.   @@#OOo*******o**°°°**o*o°°*oO#Oo##@@@@@@@@@@@@@@ @         @ @@   @° °@ @..@.@@@*   @o °@ #O *#o@. o# @@.   °@@ @   
             @@@     @OooOo***°°°******°°oooOo°°°*ooO#@@@@@@@@@@@@@@@@@@ @         @ @@   .  .  .*°° .  *  °@.*°   °°°  °°°°  @.    *@@ @   
         @@@@   @@@@Ooo*******°°**°°**°**oo°°***oOO#@@@@@#O*°°*oO#@@@@@@ @         @@@@                     .                .      °@@ @   
       O.    @@@@@@o*****. °°*°°°°°°oo*°*°***OO###@@@#o.  .°**oooO#O#@@@@ @       @@ @@*            @°       o*@     O @*           @@@@@   
         @@@@@##o**°*°°°. .*°°°*°°°*oo*°°*oO###@@@O.       .°°*oOO#@##@@@ @    @@@  @@@             @#*#o @##*O@oo@ *o*@°          #@@ @    
   O.  #O###O*°*oO**  °°  °**°°°°°°°*o*°*o##@@@O°           ..°*ooO#@@@@@ @  @@   @@@*             *@  .@°@ @°@.  @*@ °#          o@@@@@    
@°   °°°°°°***ooOo..  °  °**°.°°°°°.*OOO#@@##O#°      .°.  ..°*oooOO@@@@@O     @@@@O       °       O@**#.*O @.@o*Oo @ #*         #@@ @@     
   °*°°°°°*ooOoo°.   .°°***° .°°°°*oO#@@#O° .°o°      °oo. .°°**oO#@@@@. @ @@@@@@#°O#@@@@@@@@                                  O@@@ @@      
**°**°°..°*oOo*°..°..*°°**°°°.°*oOO#@@O°     ...     .°*Oo°°°*ooo#@@@@o # @@@@@@@@@@@@@@@@@@@°                              *@@@@  @        
***°.. .*oo*°°°  .°°**..******OO####Oo.         . .   .*O#OO*o#@###@@@@@ O   @@@@@@        @@@o                          °#@@@@o @@         
..°.  °  **       .°°..°*Oo*O####OooOO*         O**O .°*O@@@O*O@@@#o#@@@# O         @@@@@@@@ @@O                     o@@@@@@    @           
  .....   °.       °*°°*oOOOO##ooOOOOooooooo.     .°.*ooO#@@@@@@@@@@@@@@@    @@@@@@@        @ @@@@*******.     °*@@@@@@@@   @@@@            
  ..       .°°...  *ooooOO##@@#OO##O**O##Oooo°°**oooOO#@@@@@@@@@@@@@@@@@@ @                  @  @@@@@@@@@@@@@@@@@@@@@     @@                
             °*o*.   °°**oO#@@##OOOo**oO#OoO###OooO#OOOO#@@@@@@@@@@@@@@@@ @                   @@         @@@@@@      @@@@@                  
              °ooO**.   °*oO#OOOooooooooO##@@@@@@##@@@@@@@@@@@@@@@@@@@@@@ @                     @@@@@@@@@       @@@@@                       
                .*OOOo***oO####@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ @                               @@@@@@@                            
                  ***oooOO######@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ @                                                                  
                  .  .*OO##O#######@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ @                                                                  
             °. .°*°   .°oOO#####@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ @                                                                  
       .    .°°°°°°o**oo  ..°°*oOOOOOO###########@@@@@#O#@@@@@@@@@@@@@@° @                                                                  
             .°°°°  .*oooOOo*****oo**oOO########@@@@@#####@@@@@@@@@@@@@ @                                                                   
              .°°°.  .°*oo*OooOo*oooOO########@@@@@@@#@@@@@@@@@@@@@@@@# @                                                                   
            ..°°°°°°°°°**oooooO##OOOO####@@@@@@@@@@@@@@@@@@@@@o*oOO@#                                                                       
.         ............°oooooOooO###@@@@@@@@@@@@@@@@@@@##@@@@@°        @                                                                     
..       ....  ...   .°*oooooOOOooOOO#@@@@@@@@@@@#####@OooO*  O@OO@O@   
''')
    time.sleep(0.5)
    print ('''
[ MADE BY THE 7-ELEVEN HEALTH INSPECTION AGENCY & CHRIS HANSEN SWAT UNIT - 2022 ]
''')
    time.sleep(2)
    moduleCheck()
    fooBar.message = input("Enter your desired message or image link to spam: ")
    booleans.spamOn = True
    time.sleep(1)
    print ('''
EMBRACE THE CAMELS... IT'S TIME FOR THE
   _________              ________       _   ____  ____  _____
  / ____/   |  ____ ___  / ____/ /      / | / / / / / /_/ ___/
 / /   / /| | / __ `__ \/ __/ / /      /  |/ / / / / __/\__ \ 
/ /___/ ___ |/ / / / / / /___/ /___   / /|  / /_/ / /_ ___/ / 
\____/_/  |_/_/ /_/ /_/_____/_____/  /_/ |_/\____/\__//____/  
                                                              
''')
    print ("[ Press Esc to quit. Press End to toggle SprintFuck on/off. Press Insert to toggle speed. ]")

# Refreshes booleans to keep code consistent and not spaghetti as fuck
def boolReset():
    booleans.sporkyOn = False
    booleans.botPwnToggleOn = False
    BotPwn.botPwnCase = 0
    booleans.sdnOn = False
    booleans.toiletGuyOn = False
    booleans.skillIssueOn = False
    booleans.hukPobyOn = False
    booleans.hereForFrogeOn = False
    booleans.whyHeLookinOn = False
    booleans.muscleManDonutsOn = False
    booleans.pingEveryoneOn = False
    booleans.pandorasBoxOn = False

# spamMessage function writes out the chosen spam message
def spamMessage():
    if booleans.sdnOn == True:
        pyautogui.typewrite(AutoSpam.sdn)
        pyautogui.press("enter")
        time.sleep(fooBar.speed)
    elif booleans.toiletGuyOn == True:
        pyautogui.typewrite(AutoSpam.toiletGuy)
        pyautogui.press("enter")
        time.sleep(fooBar.speed)
    elif booleans.skillIssueOn == True:
        pyautogui.typewrite(AutoSpam.skillIssue)
        pyautogui.press("enter")
        time.sleep(fooBar.speed)
    elif booleans.hukPobyOn == True:
        pyautogui.typewrite(AutoSpam.hukPoby)
        pyautogui.press("enter")
        time.sleep(fooBar.speed)
    elif booleans.hereForFrogeOn == True:
        pyautogui.typewrite(AutoSpam.hereForFroge)
        pyautogui.press("enter")
        time.sleep(fooBar.speed)
    elif booleans.whyHeLookinOn == True:
        pyautogui.typewrite(AutoSpam.whyHeLookin)
        pyautogui.press("enter")
        time.sleep(fooBar.speed)
    elif booleans.muscleManDonutsOn == True:
        pyautogui.typewrite(AutoSpam.muscleManDonuts)
        pyautogui.press("enter")
        time.sleep(fooBar.speed)
    elif booleans.pingEveryoneOn == True:
        pyautogui.typewrite(AutoSpam.pingEveryone)
        pyautogui.press("enter")
        time.sleep(fooBar.speed)
    elif booleans.botPwnToggleOn == True:
        BotPwn.botPwnCmd = BotPwn.botPwnList[BotPwn.botPwnCase]
        BotPwn.botPwnSpam = BotPwn.botPwnSyntax + BotPwn.botPwnCmd
        pyautogui.typewrite(BotPwn.botPwnSpam)
        pyautogui.press("enter")
        time.sleep(fooBar.speed)
    elif booleans.sporkyOn == True:
        if booleans.sporkyAutofillOn == True:
            Sporky.sporkyRandom = random.choice(Sporky.sporkyAutofill)
            pyautogui.typewrite(Sporky.sporkyRandom)
            pyautogui.press("enter")
            time.sleep(fooBar.speed)
        else:
            pyautogui.typewrite(Sporky.sporky)
            pyautogui.press("enter")
            time.sleep(fooBar.speed)
    elif booleans.pandorasBoxOn == True:
        PandorasBox.surprise = random.choice(PandorasBox.pandorasBoxVars)
        pyautogui.typewrite(PandorasBox.surprise)
        pyautogui.press("enter")
        time.sleep(fooBar.speed)
    else:
        pyautogui.typewrite(fooBar.message)
        pyautogui.press("enter")
        time.sleep(fooBar.speed)        

# Python lacks switch cases, so I had to write my own function for it...
def speedCase():
    if fooBar.caseSwitch == 1:
        fooBar.speed = 2
        print ("[ Delay increased to 2 ]")
    elif fooBar.caseSwitch == 2:
        fooBar.speed = 3
        print ("[ Delay increased to 3 ]")
    elif fooBar.caseSwitch == 3:
        fooBar.speed = 4
        print ("[ Delay increased to 4 ]")
    elif fooBar.caseSwitch == 4:
        fooBar.speed = 0
        print ("[ Delay off ]")
    elif fooBar.caseSwitch == 5:
        fooBar.speed = 1
        fooBar.caseSwitch = 0
        print ("[ Delay reset ]")
    fooBar.caseSwitch += 1

# Turns spam on and off for when you're rampage spamming
def toggleSpam():
    if booleans.spamOn == True:
        booleans.spamOn = False
        print ("[ Toggled SprintFuck Off ]")
    else:
        booleans.spamOn = True
        print ("[ Toggled SprintFuck On ]")

# Toggles command used by BotPwn
def toggleCmd():
    BotPwn.botPwnCase += 1
    if BotPwn.botPwnCase == len(BotPwn.botPwnList):
        BotPwn.botPwnCase = 0
    else:
        pass

# on_press parses key as arg to determine user's desired function
def on_press(key):
    if key == keyboard.Key.f2:
        if booleans.discordModuleOn == True:
            if booleans.sdnOn == False:
                boolReset()
                booleans.sdnOn = True
                print ("[ SDN Enabled ]")
            else:
                pass
        else:
            pass
    elif key == keyboard.Key.f3:
        if booleans.discordModuleOn == True:
            if booleans.toiletGuyOn == False:
                boolReset()
                booleans.toiletGuyOn = True
                print ("[ Toilet Guy Enabled ]")
            else:
                pass
        else:
            pass
    elif key == keyboard.Key.f4:
        if booleans.discordModuleOn == True:
            if booleans.skillIssueOn == False:
                boolReset()
                booleans.skillIssueOn = True
                print ("[ Skill Issue Enabled ]")
            else:
                pass
        else:
            pass
    elif key == keyboard.Key.f5:
        if booleans.discordModuleOn == True:
            if booleans.hukPobyOn == False:
                boolReset()
                booleans.hukPobyOn = True
                print ("[ Huk Poby Enabled ]")
            else:
                pass
        else:
            pass
    elif key == keyboard.Key.f6:
        if booleans.discordModuleOn == True:
            if booleans.hereForFrogeOn == False:
                boolReset()
                booleans.hereForFrogeOn = True
                print ("[ Here For Froge Enabled ]")
            else:
                pass
        else:
            pass
    elif key == keyboard.Key.f7:
        if booleans.discordModuleOn == True:
            if booleans.whyHeLookinOn == False:
                boolReset()
                booleans.whyHeLookinOn = True
                print ("[ Why He Lookin'? Enabled ]")
            else:
                pass
        else:
            pass
    elif key == keyboard.Key.f8:
        if booleans.discordModuleOn == True:
            if booleans.muscleManDonutsOn == False:
                boolReset()
                booleans.muscleManDonutsOn = True
                print ("[ Muscle Man Donuts Enabled ]")
            else:
                pass
        else:
            pass
    elif key == keyboard.Key.f9:
        if booleans.discordModuleOn == True:
            if booleans.pingEveryoneOn == False:
                boolReset()
                booleans.pingEveryoneOn = True
                print ("[ Ping Everyone Enabled ]")
            else:
                pass
        else:
            pass
    elif key == keyboard.Key.f10:
        if booleans.discordModuleOn == True:
            if booleans.botPwnOn == True:
                if booleans.botPwnToggleOn == True:
                    toggleCmd()
                    print ("[ ", BotPwn.botPwnSyntax, BotPwn.botPwnList[BotPwn.botPwnCase], " Enabled ]")
                else:
                    boolReset()
                    booleans.botPwnToggleOn = True
                    print ("[ BotPwn Enabled ]")
            else:
                pass
        else:
            pass
    elif key == keyboard.Key.f11:
        if booleans.discordModuleOn == True:
            boolReset()
            booleans.sporkyOn = True
            print ("[ Sporky Enabled ]")
        else:
            pass
    elif key == keyboard.Key.f12:
        if booleans.discordModuleOn == True:
            boolReset()
            booleans.pandorasBoxOn = True
            print ("[ Pandora's Box Enabled ]")
        else:
            pass
    elif key == keyboard.Key.delete:
        if booleans.discordModuleOn == True:
            boolReset()
            print ("[ Discord Module Toggled Off ]")
        else:
            pass
    elif key == keyboard.Key.insert:
        speedCase()
    elif key == keyboard.Key.end:
        toggleSpam()
    elif key == keyboard.Key.esc:
        print ("[ Exiting SprintFuck... ]")
        fooBar.foo = False
        os._exit(0)

# Listener waits until Escape is pressed, and closes the Bot when pressed
mainMenu()
with keyboard.Listener(on_press=on_press) as listener:
    while fooBar.foo == True:
        if booleans.spamOn == True:
            spamMessage()
        else:
            pass
    listener.join()