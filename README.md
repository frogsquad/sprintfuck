## SPRINTFUCK v1.0.5
#### YEAH IT'S A SPAM BOT, DEAL WITH IT NERD

![Funny Froggy](sprintfuck.png "REEEEEEEEEEE")

SprintFuck v1.0.5 - It Only Gets Worse™

### LATEST NEWS!

![NOT THE BEES!](sprintfuckdiscordupdate.png "OH YEAH WOO YEAH!")

**INTRODUCING THE DISCORD MODULE!**
The Discord Module brings a whole new set of functions to SprintFuck v1.0.5 that make your Discord spamming experience even lulzier! The Discord Module can be enabled and configured on program startup.

###### AutoSpam: 
We have coded 8 automated spam messages (mostly imgur links) that can be activated with Keys F2 - F9, messages include:<br>
- SDN aka Shitting Dick Nipples (Image) **WARNING: NSFW**
- Toilet Guy (Image)
- Skill Issue (Animated Image)
- Huk Poby (Text)
- Here For Froge (Image)
- Why He Lookin'? (Video)
- Muscle Man Donuts (Video)
- Ping Everyone (Text)

###### BotPwn:
BotPwn is built to piss servers off where it hits best, the Bots. BotPwn is configured on startup with the desired Bot's syntax as well as the command you wish to abuse with BotPwn. Multiple command args can also be supplied such as a username to ping (must include user tag # as well) along with commands to supply to those args, which are then stored in a list that can be toggled through. Add the "ping" command for extra lulz. BotPwn can be activated with the F10 key, command toggling can be done with F10 while BotPwn is on.

###### Sporky:
Sporky is a very classic type of chat application exploit, and one of my favourites: the Fork Bomb. Enabling Sporky will start ending Fork Bombs into the chat which will crash most Discord users, softlocking them out of the chat. Sporky can also be configured upon startup to have your own Fork Bomb setting. Sporky can be activated with the F11 key.

**WARNING: SPORKY MAY POTENTIALLY CRASH YOUR DISCORD AND/OR LAG YOU OUT DEPENDING ON YOUR PLATFORM, USE SPORKY WITH CAUTION!**

###### Pandora's Box:
Do you dare open it? A series of randomly chosen messages handpicked... well... at random by us. We don't decide what the images are, we just pick them eyes closed and they go in. There might be something good, might be something bad, how else will you find out if you don't open it? Pandora's Box can be activated with the F12 key.

## FEATURES & UPDATES

- Discord Module with new features specific to Discord (as of v1.0.5)
- Insert is now the toggle speed key as End has been designated to the new toggle off feature (as of v1.0.5)
- Toggle key to change set speed if you need to avoid timeouts since custom timeout speed was practically useless (as of v1.0.4)
- Now displays within Terminal/PowerShell that you've toggled speed or quit the program (as of v1.0.4)
- Images can be spammed on web chat applications such as Discord, Guilded, or IRCs with image hyperlinking
- Focuses on avoiding timeout delay instead of spam quantity to ensure your spam is consistent
- Written in Python to reduce file size and maximize spam potential without requiring high amounts of CPU usage

![Sucks to suck](lookwhatudid.png "He wasn't having a very good day")

## HOW USE IT??? IDK HOW COMPUTER

###### Linux & MacOS:<br>
Install the needed Python libraries from the requirements.txt file, clone the git repository in your Terminal, then cd into the folder and run it with Python3.
```bash
pip3 install requirements.txt
git clone https://gitlab.com/frogsquad/sprintfuck.git)
cd sprintfuck
python3 sprintfuck.py
```

###### Windows: <br>
Install your Python interpreter of choice if your version of Windows isn't prepackaged with one, then edit your environment variables to add the path to the Python IDLE. Open your PowerShell, then type the following to install the needed libraries and run SprintFuck.
```powershell
python -m pip install requirements.txt
cd (PATH)\sprintfuck
python sprintfuck.py
```

Once you have SprintFuck's modules installed and the Python script on your computer, it's time to blast it.<br>
Pressing the Insert key toggles the speed of the spam.<br>
Pressing the End key toggles SprintFuck's spam on and off.<br>
Pressing the Esc key exits SprintFuck.

Anyways, don't be dumb with this program or any of our other programs, okay?
Make sure to read the License so you know your rights regarding Frogware.
